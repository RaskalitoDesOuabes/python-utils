#! python3
# -*- coding: utf-8 -*-

def integer_to_string(
        v:int,
        zerochar:str='0',
        minus:str='-',
        base:int=10,
        basechar10:str='A') -> str:

    """Calculates a string representing a number. The zero char and the minus can be any Unicode char. The base can different from 10.

    :param int v: The value
    :param str zerochar: The zero digit zero.
    :param str minus: The minus character
    :param int base: Base from 2 to 36. Default is 10
    :return: A string based on the numeric value and the Unicode index of the zero character.

    >>> integer_to_string(-123450)
    '-123450'
    >>> integer_to_string(-123450,'২')
    '-৩৪৫৬৭২'
    >>> integer_to_string(-4567890,'⁰','⁻')
    '⁻⁴⁵⁶⁷⁸⁹⁰'
    >>> integer_to_string(2416819951,base=16,basechar10='a')
    '900dbeef'
    >>> integer_to_string(45371,base=36,basechar10='A')
    'Z0B'
    """
    s=""
    if  isinstance(zerochar, str) and len(zerochar)==1 and \
            isinstance(basechar10, str) and len(basechar10) == 1 and \
            isinstance(v, int) and \
            isinstance(base, int) and \
            1 < base <= 36 :

        if v==0:
            s=zerochar
        else:
            if v<0 :
                s=minus
                v=-v

            bp:int = int(base)
            while bp<=v :
                bp *= base
            bp = int(bp/base)

            while bp != 1 :
                q = int(v/bp)
                v = int(v-q*bp)
                s+= chr(ord(zerochar)+q if q<=9 else ord(basechar10)+(q-10))
                bp= int(bp/base)

            s+= chr(ord(zerochar)+v if v<=9 else ord(basechar10)+(v-10))
    else:
        s=str(v)

    return s

if __name__ == '__main__':
    import sys
    print(integer_to_string(  int(sys.argv[1]), sys.argv[2], sys.argv[3], int(sys.argv[4]), sys.argv[5]))
