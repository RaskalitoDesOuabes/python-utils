
import sys
# Sources : http://ascii-table.com/ansi-escape-sequences-vt-100.php
#           http://braun-home.net/michael/info/misc/VT100_commands.htm

class SingletonVt100(type):
	_instances = {}

	def __call__(cls, *args, **kwargs):
		if cls not in cls._instances:
			instance = super().__call__(*args, **kwargs)
			cls._instances[cls] = instance
		return cls._instances[cls]

class Vt100(metaclass=SingletonVt100) :

	"""
	Container of escape sequences to emulate a Vt100 terminal.
	Done before knowing the package `colorama <https://pypi.org/project/colorama/>`_ which is better.
	"""

	def __init__(self) :
		self.enable(False)

	def text_color_red(self) : sys.stdout.write(self.esc_text_color_red)
	def text_color_cyan(self) : sys.stdout.write(self.esc_text_color_cyan)
	def text_color_green(self) : sys.stdout.write(self.esc_text_color_green)
	def text_color_yellow(self) : sys.stdout.write(self.esc_text_color_yellow)
	def text_color_grey(self) : sys.stdout.write(self.esc_text_color_grey)

	def reset(self) : sys.stdout.write(self.esc_text_color_grey)

	def send(self,_str) :
		if self.enabled :
			sys.stdout.write(_str)

	def print_err(self, _msg: str) -> None:
		self.text_color_red()
		print(f'Error. {_msg}')
		self.reset()

	def print_warning(self, _msg: str) -> None:
		self.text_color_yellow()
		print(f'Warning. {_msg}')
		self.reset()

	def print_info(self, _msg: str) -> None:
		self.text_color_cyan()
		print(f'{_msg}')
		self.reset()

	def print_success(self, _msg: str) -> None:
		self.text_color_green()
		print(f'{_msg}')
		self.reset()

	def autowrap(self,_state:bool) -> None:
		sys.stdout.write(self.esc_autowrap_on if _state else self.esc_autowrap_off)

	def enable(self,_enable) :
		self.enabled=_enable
		if  _enable :
			self.esc_style_bold_start="[1m"
			self.esc_style_bold_end="[22m"
			self.esc_style_blink="[5m"

			self.esc_cursor_blinking="[?12h"
			self.esc_cursor_static="[?12l"
			self.esc_cursor_show="[?25h"
			self.esc_cursor_hide="[?25l"

			self.esc_bg_color_black="[40m"
			self.esc_bg_color_dark_red="[41m"
			self.esc_bg_color_dark_green="[42m"
			self.esc_bg_color_dark_yellow="[43m"
			self.esc_bg_color_dark_blue="[44m"
			self.esc_bg_color_dark_magenta="[45m"
			self.esc_bg_color_dark_cyan="[46m"
			self.esc_bg_color_dark_white="[47m"

			self.esc_bg_color_grey="[100m"
			self.esc_bg_color_red="[101m"
			self.esc_bg_color_green="[102m"
			self.esc_bg_color_yellow="[103m"
			self.esc_bg_color_blue="[104m"
			self.esc_bg_color_magenta="[105m"
			self.esc_bg_color_cyan="[106m"
			self.esc_bg_color_white="[107m"

			self.esc_text_color_black="[30m"
			self.esc_text_color_dark_red="[31m"
			self.esc_text_color_dark_green="[32m"
			self.esc_text_color_dark_yellow="[33m"
			self.esc_text_color_dark_blue="[34m"
			self.esc_text_color_dark_magenta="[35m"
			self.esc_text_color_dark_cyan="[36m"
			self.esc_text_color_grey="[37m"

			self.esc_text_color_dark_grey="[90m"
			self.esc_text_color_red="[91m"
			self.esc_text_color_green="[92m"
			self.esc_text_color_yellow="[93m"
			self.esc_text_color_blue="[94m"
			self.esc_text_color_magenta="[95m"
			self.esc_text_color_cyan="[96m"
			self.esc_text_color_white="[97m"

			self.esc_reset="[0m"

			self.esc_screen_clear="[2J"
			self.esc_screen_clear_from_cursor_down="[0J"
			self.esc_screen_clear_from_cursor_up="[1J"

			self.esc_line_clear="[2K"
			self.esc_line_clear_from_cursor_right="[0K"
			self.esc_line_clear_from_cursor_left="[1K"

			self.esc_cursor_up1="[1A"
			self.esc_cursor_down1="[1B"
			self.esc_cursor_home="[H"

			self.esc_autowrap_on="[?7h"
			self.esc_autowrap_off="[?7l"
		else:
			self.esc_style_bold_start=""
			self.esc_style_bold_end=""
			self.esc_style_blink=""

			self.esc_cursor_blinking=""
			self.esc_cursor_static=""
			self.esc_cursor_show=""
			self.esc_cursor_hide=""

			self.esc_bg_color_black=""
			self.esc_bg_color_dark_red=""
			self.esc_bg_color_dark_green=""
			self.esc_bg_color_dark_yellow=""
			self.esc_bg_color_dark_blue=""
			self.esc_bg_color_dark_magenta=""
			self.esc_bg_color_dark_cyan=""
			self.esc_bg_color_dark_white=""

			self.esc_bg_color_grey=""
			self.esc_bg_color_red=""
			self.esc_bg_color_green=""
			self.esc_bg_color_yellow=""
			self.esc_bg_color_blue=""
			self.esc_bg_color_magenta=""
			self.esc_bg_color_cyan=""
			self.esc_bg_color_white=""

			self.esc_text_color_black=""
			self.esc_text_color_dark_red=""
			self.esc_text_color_dark_green=""
			self.esc_text_color_dark_yellow=""
			self.esc_text_color_dark_blue=""
			self.esc_text_color_dark_magenta=""
			self.esc_text_color_dark_cyan=""
			self.esc_text_color_grey=""

			self.esc_text_color_dark_grey=""
			self.esc_text_color_red=""
			self.esc_text_color_green=""
			self.esc_text_color_yellow=""
			self.esc_text_color_blue=""
			self.esc_text_color_magenta=""
			self.esc_text_color_cyan=""
			self.esc_text_color_white=""

			self.esc_reset=""

			self.esc_screen_clear=""
			self.esc_screen_clear_from_cursor_down=""
			self.esc_screen_clear_from_cursor_up=""

			self.esc_line_clear_from_cursor_right=""
			self.esc_line_clear_from_cursor_left=""
			self.esc_line_clear=""

			self.esc_cursor_up1=""
			self.esc_cursor_down1=""
			self.esc_cursor_home=""


			self.esc_autowrap_on = ""
			self.esc_autowrap_off = ""



