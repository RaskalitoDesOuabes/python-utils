#! python3
# -*- coding: utf-8 -*-

import sys, datetime, os

from ..exception import display_exception
from ..terminal.vt100 import Vt100
from ..time.tools import seconds_to_hhmmss, deltatimeToStr

# Log task
class LogTask :
	"""
	Allows to measure the duration of a task.
	When finished, a status can be given and the task is displayed.
	"""
	INFO=0
	SUCCESS=1
	WARNING=2
	ERROR=3

	RESERVED_DUMMY_TASK=10

	def __init__(self,_name="",_time_measurement:bool=True,_show_status:bool=True):
		"""

		:param _name: Task name
		:param _time_measurement: A boolean to
		:param _show_status:
		"""
		self.name:str=""
		self.time:datetime.datetime=None
		self.genre:int=LogTask.INFO
		self.status=""
		self.show_status=True
		self.time_measurement=True
		self.level=LogTask.INFO
		self.reset(_name,_time_measurement,_show_status)

	def __str__(self) -> str:
		return f'"{self.name}" Genre:{self.genre} [{self.time}] Status:{self.status}'

	def reset(self,_name=None,_time_measurement=True,_show_status=True):
		if _name is not None :
			self.name=str(_name)
		self.time=datetime.datetime.now()
		self.show_status=_show_status
		self.time_measurement=_time_measurement
		self.genre=LogTask.INFO
		self.status=""
		return self

	def clone(self):
		new_task=LogTask().reset(self.name,self.time_measurement,self.show_status)
		new_task.genre=self.genre
		return new_task

	def make_success(self,_status=''):
		if self.genre == LogTask.RESERVED_DUMMY_TASK: return self
		self.status=_status
		self.genre=LogTask.SUCCESS
		return self

	def make_err(self,_status=''):
		if self.genre == LogTask.RESERVED_DUMMY_TASK: return self
		self.status=_status
		self.genre=LogTask.ERROR
		return self

	def make_warning(self,_status=''):
		if self.genre == LogTask.RESERVED_DUMMY_TASK: return self
		self.status=_status
		self.genre=LogTask.WARNING
		return self

	def make_info(self,_status=''):
		if self.genre == LogTask.RESERVED_DUMMY_TASK: return self
		self.status=_status
		self.genre=LogTask.INFO
		return self

	def make_exc(self,_exc,_limit=4,_msg=None):

		if self.genre==LogTask.RESERVED_DUMMY_TASK: return self

		if _msg is None :
			_msg=str(_exc)
		else:
			_msg=str(_msg)

		return self.make_err(_msg)

	def make_exc_with_backtrace(self,_exc,_limit=4,_msg=None):

		if self.genre == LogTask.RESERVED_DUMMY_TASK: return self

		if _msg is None :
			_msg=str(_exc)
		else:
			_msg=str(_msg)

		if len(_msg)!="":
			_msg=_msg+' :\n'

		return self.make_err(str(_msg)+display_exception.to_string(_exc,_limit))


# Logger
class Logger:

	# Output mask
	STDOUT=1
	FILE=2
	CB=4

	OUTPUT_MASK=STDOUT|FILE|CB

	# Message status sent to call-back
	NEW_MESSAGE='new'
	REPETITION='repetition'
	END_OF_REPETITION='repetition end'

	SYMBOL_SET_ASCII=('info','OK  ','Warn','Err ')
	SYMBOL_SET1=('ℹ','✓','!','×')
	SYMBOL_SET_SMILEYS=('ℹ','☺','⚠','☹')
	SYMBOL_SET_EMOJI=('🛈','☼','⚡','❌')

	def __init__(
			self,
			_dir:str=".",
			_name:str="log",
			_ext:str="log",
			_maxSize:int=200*1024,
			_useVt100:bool=False,
			_horodatage:bool=True,
			_time_measurement:bool=True,
			_mask_outputs:int=STDOUT,
			_cb_output=None,
			_verbosity_level:int=0,
			_group_similar_message:int=STDOUT|FILE|CB,
			_symbols:str=SYMBOL_SET1,
			_min_displayed_log_genre:int=0,
			_time_format="%Y-%m-%d %H:%M:%S"
	):
		"""
		:param _dir:
		:param _name:
		:param _ext:
		:param _maxSize:
		:param _useVt100:
		:param _horodatage:
		:param _time_measurement:
		:param _cb_output:
		:param _mask_outputs:
		:param _verbosity_level:
		:param _group_similar_message:
		:param _symbols:
		:param _time_format: Time format compatible to time.strftime (https://docs.python.org/3/library/time.html#time.strftime)
		"""
		self.file_dir=_dir.replace("*","_").replace(">","_").replace("<","_").replace("|","_")
		self.file_name=_name.replace(".","_").replace("/","_").replace("\\","_").replace("*","_").replace(">","_").replace("<","_").replace("|","_")
		self.file_ext=_ext.replace(".","").replace("/","_").replace("\\","_").replace("*","_").replace(">","_").replace("<","_").replace("|","_")
		self.file_cpt=0
		self.file=None
		self.setVerbosity(_verbosity_level)

		self.enabled=True
		self.min_displayed_genre:int=_min_displayed_log_genre

		self.group_similar_message=_group_similar_message
		self.was_repeating=False
		self.repeating_starts=0
		self.repeating_count=0
		self.previous_task=LogTask("")

		self.maxSize=_maxSize
		self.horodatage=_horodatage
		self.time_measurement=_time_measurement
		self.setTimeFormat(_time_format)

		self.setSymbols(_symbols)
		self.setOutputs(_mask_outputs,_cb_output)

		self.vt100=Vt100()
		self.enableVt100(_useVt100)

	# noinspection PyAttributeOutsideInit
	def setTimeFormat(self,_format:str):
		now=datetime.datetime.now()
		try:
			now.strftime(_format)
			self.time_format=_format
		except:
			pass

		return self

	def setMinDisplayedGenre(self,_min:int):
		self.min_displayed_genre=_min
		return self

	# noinspection PyAttributeOutsideInit
	def setSymbols(self,_symbols):
		if _symbols is None :
			self.symbols=('','','','')
		elif not isinstance(_symbols,tuple) :
			if isinstance(_symbols, list) :
				self.symbols=tuple(_symbols)
			else:
				self.symbols = tuple([_symbols])
		else:
			self.symbols=_symbols

		return self

	def setLogFile(self, _log_file:str):
		"""
		Set the path of the log file
		:param _log_file: Path to the log file. The name will be incremented if the path exists
		:return:
		"""
		if isinstance(_log_file,str):

			from pathlib import PurePath
			purepath=PurePath(_log_file)
			path=str(purepath.parent)
			name=str(purepath.stem)
			ext=str(purepath.suffix)
			if len(ext)>0 and ext[0]=='.' :
				ext=ext[1:]

			fileConfigChanged=(
				path != self.file_dir and
				name != self.file_name and
				ext != self.file_ext
			)

			self.file_dir = path
			self.file_name = name
			self.file_ext = ext

			# reopen file is dir, name or ext changed
			if not self.__isFileClosed() and fileConfigChanged :
				self.__reopen()

		return self


	def enableGroupingForOutputs(self,_group_similar_messages:int):
		self.group_similar_message=_group_similar_messages
		return self

	def setVerbosity(self,_verbosity_level:int):
		self.verbosity_level=_verbosity_level
		return self

	def enable(self,_enabled:bool):
		self.enabled=_enabled
		return self

	def getOutputs(self)->int:
		return self.outputs

	def setOutputs(self, _mask_outputs:int,_cb_output=None):
		# noinspection PyAttributeOutsideInit
		self.outputs=_mask_outputs & Logger.OUTPUT_MASK

		# noinspection PyAttributeOutsideInit
		self.cb_output=_cb_output

		return self

	def calcFilePath(self) -> str:
		if self.file_cpt>1 :
			return self.file_dir+'/'+self.file_name+'.'+str(self.file_cpt)+'.'+self.file_ext
		else :
			return self.file_dir+'/'+self.file_name+'.'+self.file_ext

	@staticmethod
	def __createDummyTask():
		task:LogTask=LogTask("",False,False)
		task.genre=LogTask.RESERVED_DUMMY_TASK

		return task

	def createTask(self,_name="",_verbose_level=0,_time_measurement=True,_show_status=True) -> LogTask :
		doIt=True
		try:
			if _verbose_level>self.verbosity_level:
				doIt=False
		except TypeError:
			pass

		if doIt :
			return LogTask(_name,_time_measurement,_show_status)

		return Logger.__createDummyTask()

	def enableVt100(self,_enabled) :
		self.vt100.enable(_enabled)
		return self

	# Calc available file name
	def calcAvailableFilePath(self) :
		file_ok=False
		while not file_ok :
			file_path=self.calcFilePath()
			if os.path.exists(file_path) :
				if os.path.isfile(file_path) :
					if os.path.getsize(file_path)<self.maxSize :
						file_ok=True
			else :
				file_ok=True

			if not file_ok :
				self.file_cpt+=1

		# Sécurité pas bô
		if self.file_cpt>1000 :
			raise ValueError ("Unable to calc available log path with dir='"+self.file_dir+"',  dir='"+self.file_dir+"',  name='"+self.file_name+"',  ext='"+self.file_ext+"'. Max internal counter exceed max" )

		return self.calcFilePath()

	def close(self):
		self.message("Log end")
		self.__close()

	def __reopen(self) :

		if self.outputs & Logger.FILE:
			self.vt100.print_warning("(!) Log file [re]opened\n")

			prev_file_path=self.calcFilePath()
			next_file_path=self.calcAvailableFilePath()

			if not self.__isFileClosed() :
				self.file.write("\n\n> next file is " + next_file_path + "\n")
				self.__close()

			self.__open()

			self.file.write("> Previous file is "+prev_file_path+"\n")

	# Reopen when working if too long
	def __reopenIfTooLong(self) :

		if self.outputs & Logger.FILE:
			file_path=self.calcFilePath()
			file_size=os.path.getsize(file_path)
			if file_size>=self.maxSize :
				self.vt100.print_warning("(!) Log file closed. Too big : %s has %d bytes\n"%(file_path,file_size))
				self.__reopen()

	def __open(self):
		file_path=self.calcAvailableFilePath()

		try:
			self.file=open(file_path,"at", encoding="utf-8")
			self.file.write("\n> Log file is %s:\n" % file_path)
			self.file.write("> Opened on "+str(datetime.datetime.now())+"\n")
			self.file.write("> UTF-8 test: [Fr]àçè [Gr]εάξ [Jp]大丈夫 [Ar]حسنًا\n")

			self.vt100.print_success("(i) Log file is '%s'" % str(file_path))

		except Exception as err:
			self.vt100.print_err("(!) Failed to open log file '%s'" % str(file_path))

	def __isFileClosed(self):
		return self.file is None or self.file.closed

	def __mayReportToFile(self):
		return self.outputs&Logger.FILE

	def __mayReportToStdOut(self):
		return self.outputs&Logger.STDOUT

	def __mayReportToCb(self):
		return self.outputs&Logger.CB

	def __canReportToFile(self):
		return self.__mayReportToFile() and not self.__isFileClosed()

	def __canReportToCb(self):
		return self.__mayReportToCb() and self.cb_output is not None

	def __close(self):
		if not self.__isFileClosed():
			self.file.close()
			self.file=None

	def message(self, _msg, _verbose_level:int=0) -> None:
		self.report(self.createTask(_msg,_verbose_level,False,False))

	def info(self, _msg, _verbose_level:int=0) -> None:
		self.message(_msg,_verbose_level)

	def warning(self, _msg, _verbose_level:int=0) -> None:
		self.report(self.createTask(_msg,_verbose_level,False,False).make_warning())

	def error(self, _msg, _verbose_level:int=0) -> None:
		self.report(self.createTask(_msg,_verbose_level,False,False).make_error())

	def report(self, _task:LogTask) -> None:

		if self.outputs==0 or not self.enabled or _task is None or _task.genre==LogTask.RESERVED_DUMMY_TASK:
			return

		if _task.genre < self.min_displayed_genre:
			return

		if self.outputs & Logger.FILE :
			if self.__isFileClosed():
				self.__open()

			self.__reopenIfTooLong()

		# Message égal au précédent ?
		isSameMessage = (
			self.previous_task.name==_task.name and
			self.previous_task.status==_task.status and
			self.previous_task.genre==_task.genre
		)

		# Drapeau indiquant les canaux utilisés pour le message
		messageOutputDone=0

		# Cas de messages similaires qu'on peut grouper.
		if self.group_similar_message!=0 and isSameMessage  :

			# If first repeat, retains repeat start time
			if not self.was_repeating :
				self.repeating_starts=datetime.datetime.now()

			# Update repeat counter
			self.repeating_count+=1

			# Send repetion information
			if (self.group_similar_message&Logger.FILE)!=0 and self.__canReportToFile():
				if not self.was_repeating:
					self.file.write("(")
				self.file.write(".")
				messageOutputDone |= Logger.FILE

			if (self.group_similar_message&Logger.STDOUT)!=0 and self.__mayReportToStdOut():
				if not self.was_repeating:
					sys.stdout.write("(")
				sys.stdout.write(".")
				messageOutputDone |= Logger.STDOUT

			if (self.group_similar_message&Logger.CB)!=0 and self.__canReportToCb():
				self.cb_output(Logger.REPETITION, {'task_name':_task.name,'task_genre':_task.genre, 'task_starts':_task.time, 'repetition_starts':self.repeating_starts, 'repetition_count':self.repeating_count})
				messageOutputDone |= Logger.CB

			#
			self.was_repeating=True

		# Test de continuité : En fonction de ce qui vient d'être fait (ou pas) et de ce qu'on devrait faire.
		# (i) Ce qui a été éventuellement fait ne concerne que le cas où on groupe les messages.
		shouldReportToFile	= (messageOutputDone&Logger.FILE  )==0 and (self.group_similar_message&Logger.FILE  )!=0 and self.__canReportToFile()
		shoudReportToStdOut	= (messageOutputDone&Logger.STDOUT)==0 and (self.group_similar_message&Logger.STDOUT)!=0 and self.__mayReportToStdOut()
		shouldReportToCB	= (messageOutputDone&Logger.CB    )==0 and (self.group_similar_message&Logger.CB    )!=0 and self.__canReportToCb()

		if shouldReportToFile or shoudReportToStdOut or shouldReportToCB :

			#
			status=_task.status

			# Horodatage
			strDateTime=""
			if self.horodatage :
				strDateTime=_task.time.strftime(self.time_format)
				strDateTime+='\t'

			# If was repeating, close the grouping
			if not isSameMessage and self.was_repeating:

				time_delta=datetime.datetime.now()-self.repeating_starts

				if shoudReportToStdOut or shouldReportToFile:
					repeat_length = "Same message %d time%s during %s" % (
						self.repeating_count,
						's' if self.repeating_count>1 else '',
						deltatimeToStr(time_delta)
					)

				if shoudReportToStdOut:
					print(f'\r{self.vt100.esc_text_color_grey}{self.vt100.esc_line_clear}{repeat_length}  {self.vt100.esc_reset}')

				if shouldReportToFile:
					self.file.write(f")\n{repeat_length}\n")

				if shouldReportToCB:
					self.cb_output(Logger.END_OF_REPETITION, {'repetition_count':self.repeating_count,'repetition length':time_delta})

			#
			if not isSameMessage :
				self.was_repeating = False
				self.repeating_count=0

			#
			delta:datetime.timedelta=None
			if _task.time_measurement and self.time_measurement:
				delta=datetime.datetime.now()-_task.time

			# Add msg to file
			if (messageOutputDone&Logger.FILE)==0 and self.__canReportToFile():
				prefix=" " if strDateTime != "" else ""
				symbol=self.getSymbol(_task.genre)

				full_msg=f"{strDateTime}{prefix}{symbol} {_task.name} {status}"
				if delta is not None:
					full_msg+=' ('+deltatimeToStr(delta)+')'
				full_msg += '\n'

				self.file.write(full_msg)
				self.file.flush()

			# Add msg to stdout
			if (messageOutputDone&Logger.STDOUT)==0 and self.__mayReportToStdOut():
				color_time=self.vt100.esc_text_color_grey
				color_text=self.vt100.esc_text_color_white
				color_status=self.vt100.esc_text_color_cyan

				prefix=" " if strDateTime != "" else ""
				symbol=self.getSymbol(_task.genre)

				if _task.genre == LogTask.SUCCESS:
					color_status = self.vt100.esc_text_color_green
				elif _task.genre == LogTask.WARNING:
					color_status = self.vt100.esc_text_color_yellow
				elif _task.genre == LogTask.ERROR:
					color_status=self.vt100.esc_text_color_red

				full_msg=f"{self.vt100.esc_line_clear}{color_time}{strDateTime}{prefix}{color_status}{symbol} {color_text}{_task.name} {color_status}{status}"
				if delta is not None:
					full_msg+=color_time+' ('+deltatimeToStr(delta)+')'
				full_msg+=self.vt100.esc_reset

				print(full_msg)

			# Add msg to CB
			if (messageOutputDone&Logger.CB)==0 and self.__canReportToCb() :
				self.cb_output(Logger.NEW_MESSAGE, {'task_name':_task.name,'task_genre':_task.genre, 'task_starts':_task.time, 'duration':delta})

		#
		self.previous_task = _task.clone()

	def getSymbol(self,_genre:int) -> str:
		sym=''
		if self.symbols is not None:
			try:
				sym=self.symbols[_genre]
			except IndexError:
				pass

		return sym

# Instance de logger
log=Logger()
def get_logger():
	return log



if __name__ == "__main__":
	import time
	log=Logger(_name="logger_test",_useVt100=True)\
		.setLogFile('test.log')\
		.setOutputs(Logger.FILE|Logger.STDOUT,lambda t,m:print(f'{str(t):15}  {str(m)}'))\
		.enableGroupingForOutputs(Logger.FILE)\
		.setSymbols(Logger.SYMBOL_SET_EMOJI)\
		.setTimeFormat("%X")

	log.info("Msg1")
	log.info("Msg1")
	log.info("Msg1")
	log.info("Msg2")
	log.info("Msg2")
	log.info("Msg1")

	taskMain=log.createTask("Main")

	task=log.createTask("Task1")
	log.report(task);

	task.reset("Task1").make_warning("becomes a warning")
	log.report(task); 
	log.report(task);
	log.report(task);

	task.make_info("becomes an info");
	log.report(task);
	log.report(task);

	task.make_err("Argh")
	log.report(task);

	task.reset("Task2")
	log.report(task);
	log.report(task);

	task.make_err()
	log.report(task);
	log.report(task);
	log.report(task);
	log.report(task);

	log.report(taskMain);

	log.close()


