import sys
import traceback

def display(_exc,_limit=None):
    print('-'*40)
    traceback.print_exc(limit=_limit)
    for arg in _exc.args:
        print('• ',arg)
    print('-'*40)

def to_string(_exc,_limit=None,_margin=2):
    exc_type, exc_value, exc_traceback = sys.exc_info()
    this_str=str(exc_type.__doc__)
    if this_str is None or this_str=='None':
        this_str=str(_exc)
    this_str += '\n'
    cs=traceback.extract_tb(exc_traceback,_limit)
    cs_margin=_margin
    cs_dr_char='  '
    #max_stack=len(exc_traceback.tb_frame)
    for cs_item in cs:
        this_str=this_str+'  '*cs_margin+cs_dr_char+cs_item.filename+'('+str(cs_item.lineno)+') : '+cs_item.line+'\n'
        cs_margin+=2
        cs_dr_char='└─ '
    this_str=this_str+'  '*cs_margin+cs_dr_char+'…'+'\n'
    for val_item in exc_value.args:
        this_str=this_str+'  '*_margin+'• '+str(val_item)+'\n'
    return this_str

