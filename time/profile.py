#! python3
# -*- coding: utf-8 -*-

import datetime

def _deltatimeToStr(_dt:datetime.timedelta) -> str :
    sdt="0s"
    dt=int(_dt.seconds*1E6+_dt.microseconds)
    if dt==0:
        pass
    elif dt<1E3 :
        sdt=str(dt)+"µs"
    elif dt<1E6 :
        sdt="%.3f"%(dt/1E3)+"ms"
    elif dt<60E6 :
        sdt="%.3f"%(dt/1E6)+"s"
    elif dt<3600E6 :
        m=int(dt/60E6)
        dt=dt-m*60E6
        sdt=str(m)+"min, "+"%.3f"%(dt/1E6)+"s"
    else :
        h=int(dt/3600E6)
        dt=dt-h*3600E6
        m=int(dt/60E6)
        dt=dt-m*60E6
        sdt=str(h)+"h, "+str(m)+"min, "+"%.3f"%(dt/1E6)+"s"
    return sdt


try:
    from utils.time.tools import deltatimeToStr
    _deltatimeToStr=deltatimeToStr
except ModuleNotFoundError:
    pass

class ProfilerTask :
    def __init__(self,_title) :
        self.start=datetime.datetime.now()
        self.end=0
        self.title=_title
        self.array=[]

    def close(self,_end) :
        self.end=_end

        childSize=len(self.array)
        if childSize>0 :
            self.array[childSize-1].close(_end)


    def print(self,_dec=0) :
        dt=self.end-self.start
        print(" "*_dec+self.title+" : "+_deltatimeToStr(dt)+f'  {self.start} -> {self.end}')

        for child in self.array :
            child.print(_dec+2)


class Profiler :
    def __init__(self) :
        self.array=[]
        self.lastStartedTask=0
        self.nbItems=0

    def clear(self) :
        self.array.clear()
        self.lastStartedTask=0
        self.nbItems=0

    def startTask(self,_title,_parentTask=0) :

        # Choose array
        arr=[]
        if _parentTask==0 :
            arr=self.array
        else :
            arr=_parentTask.array

        # Add this new entry and set its start time
        self.lastStartedTask=ProfilerTask(_title)
        arr.append(self.lastStartedTask)

        # End of the previous is start of this one
        arrSize=len(arr)
        if arrSize>1 :
            arr[arrSize-2].close(self.lastStartedTask.start)

        #
        self.nbItems+=1
        return self.lastStartedTask

    def print_and_reset(self) :
        # Close all
        arrSize=len(self.array)
        if arrSize>0 :
            self.array[arrSize-1].close(datetime.datetime.now())

        for item in self.array :
            item.print()

        self.clear()

if __name__ == "__main__":
    from time import sleep
    prf=Profiler()
    t1=prf.startTask("t1");
    st11=prf.startTask("t1.1",t1); sleep(0.5)
    st12=prf.startTask("t1.2",t1); sleep(0.5)
    t2=prf.startTask("t2"); sleep(0.5)
    t3=prf.startTask("t3");
    t31=prf.startTask("t3.1",t3)
    t311=prf.startTask("t3.1.1",t31); sleep(0.5)
    t312=prf.startTask("t3.1.2",t31); sleep(0.5)
    t32=prf.startTask("t3.2",t3); sleep(0.5)
    t4=prf.startTask("t4"); sleep(0.5)
    t5=prf.startTask("t5"); sleep(0.5)
    t6=prf.startTask("t6"); sleep(0.5)
    prf.print_and_reset()
