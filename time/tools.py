#! python3
# -*- coding: utf-8 -*-

import datetime

def deltatimeToStr(_dt:datetime.timedelta)->str :
    """Converts a time delta to readable string

    :param datetime.timedelta _dt: A time delta
    :return: A string representing this time delta
    """
    sdt="0s"
    dt=int(_dt.seconds*1E6+_dt.microseconds)
    if dt==0:
        pass
    elif dt<1E3 :
        sdt=str(dt)+"µs"
    elif dt<1E6 :
        sdt="%.3f"%(dt/1E3)+"ms"
    elif dt<60E6 :
        sdt="%.3f"%(dt/1E6)+"s"
    elif dt<3600E6 :
        m=int(dt/60E6)
        dt=dt-m*60E6
        sdt=str(m)+"min, "+"%.3f"%(dt/1E6)+"s"
    else :
        h=int(dt/3600E6)
        dt=dt-h*3600E6
        m=int(dt/60E6)
        dt=dt-m*60E6
        sdt=str(h)+"h, "+str(m)+"min, "+"%.3f"%(dt/1E6)+"s"
    return sdt

def seconds_to_hhmmss(_seconds:int)->str:
    """Transforms a integer value in seconds to a readable string

    :param _seconds: An integer value representing a number of seconds
    :return: A readable string showing the hours, minutes and seconds
    """
    str=""
    if(_seconds<60):
        str="%d sec" % _seconds
    elif _seconds<3600:
        min=_seconds//60
        _seconds=_seconds-min*60
        str="%d min, %d sec" % (min,_seconds)
    else:
        hr=_seconds//3600
        _seconds=_seconds-hr*3600
        min=_seconds//60
        _seconds=_seconds-min*60
        str="%d h, %d min, %d sec" % (hr,min,_seconds)

    return str


if __name__ == "__main__":
    print(deltatimeToStr(datetime.timedelta(0,0,12)))
    print(deltatimeToStr(datetime.timedelta(0,0,12345)))
    print(deltatimeToStr(datetime.timedelta(0,0,12345678)))
    print(deltatimeToStr(datetime.timedelta(0,0,120E6+30E6+123456)))
    print(deltatimeToStr(datetime.timedelta(0,0,3600E6+120E6+30E6+123456)))

