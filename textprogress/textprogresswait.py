# -*- coding: utf-8 -*-
import sys
from time import sleep

def asciiProgressWait(_refresh_delay,_C=40,_blocks="░▒▓█",_title="") :
    sleep_total=_refresh_delay
    sleep_count=_C  # Nb blocs
    sleep_len=sleep_total/sleep_count
    if _blocks=="" : _blocks="░▒▓█"
    nbBlocks=len(_blocks)
    for c in range(0,sleep_count,1):
        n=int(c*(nbBlocks)/(sleep_count))
        progress_char=_blocks[n]
        sys.stdout.write(_title+"│"+(c)*progress_char+(sleep_count-1-c)*" "+"│ \r")
        sleep(sleep_len)
    sys.stdout.write(len(_title)*" "+(sleep_count)*" "+" \r")
    sleep(sleep_len)
