# -*-coding:utf-8-*
import sys,datetime

class TextProgress :
	"""A class to display a progress bar based on user characters.
	The number of steps, the length of the progress bar and the edge and bar characters are user choices.
	The number of refreshes can be set to optimize the processor time used by the progress bar.
	"""
	LINES_LIGHT="├┤ ─"
	LINES_BOLD="┣┫ ━"
	BLOCKS="  ░▓"
	RECTANGLES="  ◻◼"
	THIN_RECTANGLES="  ▭▬"
	CIRCLES="  ○◉"

	def __init__(self,
				 nb_refreshs:int,
				 nb_iterations:int,
				 title:str="",
				 cursor:str='─',
				 blank:str=' ',
				 start:str='├',
				 end:str='┤',
				 chars:str=None,
				 vt100_clear_line:str="",
				 vt100_text_color_status:str= "",
				 vt100_text_color_title:str= "",
				 vt100_text_color_progress:str= "",
				 vt100_reset:str="",
				 end_status:str='Done',
				 show_delay:bool=True
				 ):

		self.title=str(title)
		self.end_title=str(end_status) if end_status is not None else ''
		self.nb_refreshs = max(2,nb_refreshs)
		self.nb_iterations = nb_iterations
		self.show_delay = True if show_delay is True else False

		self.__on_restart()

		self.vt100_clear_line = vt100_clear_line if vt100_clear_line is not None else ''
		self.vt100_text_color_status = vt100_text_color_status if vt100_text_color_status is not None else ''
		self.vt100_text_color_title = vt100_text_color_title if vt100_text_color_title is not None else ''
		self.vt100_text_color_progress = vt100_text_color_progress if vt100_text_color_progress is not None else ''
		self.vt100_reset = vt100_reset if vt100_reset is not None else ''

		self.container_start=start
		self.container_stop=end
		self.cursor_blank=blank
		self.cursor=cursor
		self.setChars(chars)

	def __on_restart(self):
		self.start=datetime.datetime.now() if self.show_delay is True else None
		self.step = self.nb_iterations / self.nb_refreshs
		self.cur_iteration = int(0)
		self.next_step = 0
		self.cur_nb_steps = 0
		self.str = ""
		self.closed=False


	def restart(self,
				nb_refreshs:int,
				nb_iterations:int,
				title: str = "") -> object:
		"""Reset the progress bar with a new number of refreshs and iteration and a new title

				:param nb_refreshs: Number of refreshes
				:param nb_iterations: The expected number of iterations
				:param title: The title
				:returns: this object
				:rtype: TextProgress
		"""
		self.close()

		self.title=str(title)
		self.nb_refreshs = nb_refreshs
		self.nb_iterations = nb_iterations

		self.__on_restart()

		return self

	def setChars(self, chars: str) -> object:
		"""Customizes the progress bar by giving the characters for the edges, the
			blank and the fill characters

			:param str chars: A string of 4 characters '<begin char><end char><blank char><fill char>'
			:returns: this object
			:rtype: TextProgress
		"""
		if isinstance(chars, str) and len(chars) == 4:
			self.container_start = chars[0]
			self.container_stop = chars[1]
			self.cursor_blank = chars[2]
			self.cursor = chars[3]

		return self

	def showDelay(self,showDelay:bool)->object:
		"""Set the `show delay` property.

				:param bool showDelay: If True, the duration measured from the bar open/close is displayed
				:returns: this object
				:rtype: TextProgress
		"""
		self.show_delay=showDelay
		return self

	def refresh(self,iteration:int,status:str=""):
		"""This method must be called at each iteration to update the progress bar

				:param int iteration: The current iteration. Should not be greater than the number of iterations given at the initialization
				:param str status: A status to display with the progress bar
		"""
		self.cur_iteration=iteration
		if iteration >= self.next_step:
			self.next_step += self.step
			self.cur_nb_steps = self.nb_refreshs * iteration / self.nb_iterations
			self.str = ('\r'+
						self.vt100_clear_line +
						self.vt100_text_color_title +
						self.title +
						self.vt100_text_color_progress +
						self.container_start +
						self.cursor * int(self.cur_nb_steps) +
						self.cursor_blank * int(self.nb_refreshs - int(self.cur_nb_steps)) +
						self.container_stop + ' ' +
						f'{int(self.cur_nb_steps / self.nb_refreshs * 100):3}% ' +
						self.vt100_text_color_status +
						status + '\r')
			sys.stdout.write(self.str)
			sys.stdout.flush()

	def advance(self,status:str=""):
		"""Wrapper to the :py:meth:`refresh` method. The current iteration is just incremented

		:param str status: A status to be displayed with the progress bar
		"""
		self.cur_iteration+=1
		self.refresh(self.cur_iteration,status)

	def close(self,show_final_stat:bool=True):
		"""Close the progress bar. The line is cleared from the begining.
		If the member `show_final_stat` is True, the *end status* given at
		the initialization is displayed with the duration (if required).

		:param show_final_stat: A final display showing the status and optionally the duration is displayed
		"""

		if self.closed:
			return

		self.advance() # To reach 100%

		if self.vt100_clear_line is None or self.vt100_clear_line=="":
			sys.stdout.write("   " + ' ' * self.nb_refreshs + '  ' + '     \r')
		else:
			sys.stdout.write(f'{self.vt100_clear_line}')

		if show_final_stat:
			# dT
			strDelta = ""
			if self.show_delay:
				from ..time.tools import deltatimeToStr
				delta = datetime.datetime.now() - self.start
				strDelta = f' {self.vt100_text_color_progress}({deltatimeToStr(delta)})'

			# End msg
			if len(self.title)>0 :
				sys.stdout.write(self.title+' ')

			print(f'{self.vt100_text_color_title}{self.end_title}{strDelta}{self.vt100_reset}')

		self.closed=True

if __name__ == '__main__' :

	NB_ITERATIONS=100000

	tp=TextProgress(20,NB_ITERATIONS,'Boucle')
	for i in range(0,NB_ITERATIONS) :
		tp.refresh(i)

	tp.restart(20,NB_ITERATIONS)
	for i in range(0,NB_ITERATIONS) :
		tp.refresh(i)

	tp.restart(20,NB_ITERATIONS).showDelay(False).setChars(TextProgress.BLOCKS)
	for i in range(0,NB_ITERATIONS) :
		tp.refresh(i)
	tp.close()


