"""
This module allows to simplify the definition, the display and the interpretation of command
line arguments throuh the :py:class:`CliSyntax` class.
"""

import os, sys
from typing import List, Optional


class CliSyntaxException(Exception):
    """Syntax class exception
    """
    def __init__(self, _message):
        self.message = str(_message)
        super().__init__(self.message)

    def __str__(self):
        return "CLI argument error - " + self.message


class ArgInterpreter:
    """Base class of argument interpreters.
    Such an interpreter is associated with an :py:class:`option <CliOption>` argument through the
    :py:meth:`CliOption.set_interpreter` is called and may raise a :py:class:`CliSyntaxException` exception when handling the argument.

    The sub-classes :py:class:`ArgInterpreterEnum`, :py:class:`ArgInterpreterNum`, :py:class:`ArgInterpreterInt` and :py:class:`ArgInterpreterFloat` can also be used.
    """

    def __init__(self,arg_type,interpret_fn=None):
        self.arg_type=arg_type
        self.interpret_fn=interpret_fn

    def handle(self, arg: str) -> object:
        """
        Handles the :py:class:`option <CliOption>` of an argument. The associated interpreter declared with :py:meth:`CliOption.set_interpreter` is called and may raise a :py:class:`CliSyntaxException` exception.

        :raises CliSyntaxException: through the associated interpreter.
        :param arg: The argument option
        :return: The argument option
        """
        if self.interpret_fn is not None:
            return self.interpret_fn(arg)
        return arg

    def stringify(self):
        return str(self.arg_type)



class ArgInterpreterEnum(ArgInterpreter):
    """Argument interpreter for enums
    """
    def __init__(self,enums):
        """[summary]

        :param enums: An iterable containing the enum objects
        :type enums: Iterable
        """
        from collections.abc import Iterable
        self.enums=None
        if enums is None:
            self.enums=()
        elif not isinstance(enums,Iterable):
            self.enums=tuple(enums)
        else:
            self.enums=enums

        super().__init__(object,lambda o: o in self.enums)

    def handle(self, arg) -> object:
        """Handle an argument

        :param arg: The CLI argument
        :raises CliSyntaxException: This exception is raised if the argument is not in the enum list
        :return: The input arg
        """
        if arg in self.enums:
            return arg
        raise CliSyntaxException(f"'{arg}' is not is the list {str(self.enums)}")

    def stringify(self):
        """Returns a descriptor if this interpreter

        :return: Description of the expected value
        :rtype: str
        """
        return f'Value among {self.enums}'


class ArgInterpreterNum(ArgInterpreter):
    """Argument interpreter of numeric value.

    Sub-class of :py:class:`ArgInterpreter`.
    """
    @staticmethod
    def to_int(obj):
        """Extract integer value from an object

        :param obj: Incoming object
        :type obj: any
        :raises CliSyntaxException: This exception is raised if the incoming object can not be transformed into integer value
        :return: integer representation of the object
        :rtype: int
        """
        try:
            return int(obj)
        except:
            raise CliSyntaxException(f"Value '{obj}' is not an integer")

    @staticmethod
    def to_float(obj):
        """Extract float value from an object

        :param obj: Incoming object
        :type obj: any
        :raises CliSyntaxException: This exception is raised if the incoming object can not be transformed into float value
        :return: float representation of the object
        :rtype: float
        """
        try:
            return float(obj)
        except:
            raise CliSyntaxException(f"Value '{obj}' is not a float value")

    def __init__(self,arg_type,min_val=None,max_val=None,interpret_fn=None):
        """Ctor

        :param arg_type: numeric value type
        :type arg_type: type
        :param min_val: minimum value, defaults to None
        :type min_val: type, optional
        :param max_val: maximum value, defaults to None
        :type max_val: type, optional
        :param interpret_fn: the interpreter function, defaults to None
        :type interpret_fn: function, optional
        """
        super().__init__(arg_type,interpret_fn)

        self.min_val=min_val
        self.max_val=max_val

    def check_min_max(self,val):
        """
        Check if a value is in the min/max range

        :raises CliSyntaxException: if the value is out of range
        :param val:
        :return: The input value
        """
        if val is not None :
            if self.min_val is not None:
                if val<self.min_val :
                    raise CliSyntaxException(f"Value {val} too little. Min is {self.min_val}")
            if self.max_val is not None:
                if val>self.max_val :
                    raise CliSyntaxException(f"Value {val} too big. Max is {self.max_val}")

        return val

    def stringify(self)->str:
        """Makes this object readable

        :returns: Readable presentation of this object
        :rtype: str
        """
        displayed_type:str=self.str_type()
        if self.min_val is None and self.max_val is None:
            return f'{displayed_type} value'
        if self.min_val is None:
            return f'{displayed_type} value <= {str(self.max_val)}'
        if self.max_val is None:
            return f'{displayed_type} value >= {str(self.min_val)}'
        return f'{displayed_type} value in [{str(self.min_val)},{str(self.max_val)}]'

    def str_type(self):
        return 'Numeric'

class ArgInterpreterInt(ArgInterpreterNum):
    """
    Sub-class of :py:class:`ArgInterpreterNum` to interpret an integer value.
    """

    def __init__(self,min_val=None,max_val=None):
        super().__init__(int, min_val, max_val, ArgInterpreterNum.to_int)

    def handle(self,arg):
        """Handle an argument

        :param arg: The CLI argument
        :raises CliSyntaxException: This exception is raised if the argument can not be transformed into an integer or the value is out of range.
        :return: The input arg
        """

        val=ArgInterpreter.handle(self, arg)
        return super().check_min_max(val)

    def str_type(self):
        return 'Integer'


class ArgInterpreterFloat(ArgInterpreterNum):
    """
    Sub-class of :py:class:`ArgInterpreterNum` to interpret a floating-point value.
    """

    def __init__(self,min_val=None,max_val=None):
        super().__init__(float, min_val, max_val, ArgInterpreterNum.to_float)

    def handle(self,arg):
        """Handle an argument

        :param arg: The CLI argument
        :raises CliSyntaxException: This exception is raised if the argument can not be transformed into a float or the value is out of range.
        :return: The input arg
        """
        val = ArgInterpreter.handle(self, arg)
        return super().check_min_max(val)

    def str_type(self):
        return 'Float'




class CliOption:
    """
    This class defines a parameter option
    """
    short_name = None
    long_name = None
    param_name = None
    desc = None
    cb = None
    interpreter = None

    def __init__(self, sn, ln, pn, de, cb, interp):
        self.short_name = sn
        self.long_name = ln
        self.param_name = pn
        self.desc = de
        self.cb = cb
        self.interpreter = interp

    def set_interpreter(self,interp:ArgInterpreter):
        """
        Associates an interpreter to the option. This interpreter must be an instance of a class inherited from :py:class:`ArgInterpreter`.

        :param ArgInterpreter interp: Interpreter. The type of this interpreter must be a sub-class of class :py:class:`ArgInterpreter`
        """
        self.interpreter = interp

    def get_interpreter(self):
        """
        Returns the associated interpreter

        :return: The associated interpreter
        :rtype: ArgInterpreter
        """
        return self.interpreter

    def assume_int(self,min_val=None,max_val=None):
        """
        Associates an instance of :py:class:`ArgInterpreterInt`, assuming that this option should be an integer value.

        :param min_val: Required min value for the parameter option
        :param max_val: Required max value for the parameter option
        """
        self.set_interpreter(ArgInterpreterInt(min_val,max_val))

    def assume_float(self,min_val=None,max_val=None):
        """
        Associates an instance of :py:class:`ArgInterpreterFloat`, assuming that this option should be a float value.

        :param min_val: Required min value for the parameter option
        :param max_val: Required max value for the parameter option
        """
        self.set_interpreter(ArgInterpreterFloat(min_val,max_val))

    def assume_enum(self,enums):
        """
        Associates an instance of :py:class:`ArgInterpreterEnum`, assuming that this option should be a value of the enum.

        :param enums: Set of acceptable values for the parameter option
        """
        self.set_interpreter(ArgInterpreterEnum(enums))

    def interpret_arg(self,arg:object)->object:
        """
        Interprets an argument. The associated interpreter may raise a :py:class:`CliSyntaxException`

        :raise CliSyntaxException: If an associated :py:class:`interpreter <ArgInterpreter>` finds an error.
        :param arg: The argument to interpret
        :return: The input argument
        """
        if self.interpreter :
            try:
                return self.interpreter.handle(arg)
            except CliSyntaxException as err:
                err_msg=f"Error while interpreting -{self.short_name} '{arg}' : "+err.message
                raise CliSyntaxException(err_msg)
        return arg


class CliParam:
    """Defines a parameter, *i.e* a simple string
    """
    name = None
    desc = None
    found_val = None

    def __init__(self, name : str, desc : str):
        self.name = name
        self.desc = desc
        self.found_val = ""

class CliSyntax:
    """
    Defines the exhaustive required CLI syntax, *i.e* a set of :py:class:`CliParam` parameters and/or :py:class:`CliOption` options.
    """
    options = []
    params = []
    optional_params = []

    cb_info = print
    cb_warning = print
    cb_error = print
    cb_arg_error = None

    need_recalc = True

    found_options : str = ""
    found_options_args = {}
    found_params: List[str] = []

    def __init__(self, cb_info=None, cb_warning=None, cb_error=None):
        """
        Define a CLI arguments handler

        :param cb_info: Call-back to print normal message. Default is :py:func:`print`)
        :param cb_warning: Call-back to print warning message (default:print)
        :param cb_error: Call-back to print error message (default:print)
        """
        self.help_string = ""
        self.opt_strings=["",[]]
        self.cb_arg_error = self.arg_error
        self.cb_info = cb_info if cb_info is not None else print
        self.cb_warning = cb_warning if cb_warning is not None else print
        self.cb_error = cb_error if cb_error is not None else print
        self.dummy_option = CliOption('?', '?', None, 'default reserved dummy option', None, None)

        self.add_option('h',long_name='help',desc="Display this page and exits",cb=lambda param: self.print_syntax())

    def get_option_arg(self, short_name:str, def_value: object=None) -> object :
        """
        Returns the argument of an option or a default value if the option is not found (*i.e* not in the command line arguments).

        :raises KeyError: if the short name does not define any option
        :param short_name: The short name of the option
        :param def_value: The default value returned if the option is not found.
        :return: The given argument or the default value.
        """
        try:
            return self.found_options_args[short_name]
        except KeyError:
            return def_value

    def get_option(self, short_name:str) -> Optional[CliOption]:
        """
        Retrieves a :py:class:`CliOption` option associated with a short name

        :param short_name: A single character used to declare an :py:class:`option <CliOption>` through :py:meth:`CliSyntax.add_option`
        :return: The :py:class:`CliOption` option associated with `short_name` or None if short_name is not associated with any option.
        :rtype: CliOption
        """
        if isinstance(short_name,str) :
            if len(short_name)>0 :
                if short_name[0]=='-':
                    if len(short_name)!=2:
                        return None
                    short_name=short_name[1]
                for opt in self.options:
                    if opt.short_name == short_name:
                        return opt
        return None

    def get_option_by_long_name(self, long_name:str) -> Optional[CliOption]:
        """
        Retrieves an :py:class:`option <CliOption>` associated with a long name

        :param long_name: A string used to declare an :py:class:`option <CliOption>` through :py:meth:`CliSyntax.add_option`
        :return: The :py:class:`CliOption` option associated with ``long_name`` or `None` if ``short_name`` is not associated with any option.
        :rtype: CliOption
        """
        if isinstance(long_name,str) :
            if len(long_name)>0 :
                if long_name[0:2]=='--':
                    if len(long_name) <= 2:
                        return None
                    long_name=long_name[2:]
                for opt in self.options:
                    if opt.long_name is not None and opt.long_name == long_name:
                        return opt
        return None

    def get_param(self, param_name) -> Optional[CliParam]:
        """
        Returns a :py:class:`CliParam` parameter declared through :py:meth:`add_param`

        :param param_name: The name associated with the parameter
        :return: :py:class:`CliParam` parameter associated with param_name
        :rtype: CliParam
        """
        for param in self.params:
            if param.name == param_name:
                return param

        for param in self.optional_params:
            if param.name == param_name:
                return param

        return None

    def add_option(self, short_name:str, desc:str=None, long_name:str=None, param_name:str=None, cb=None) -> CliOption:
        """Adds an :py:class:`option <CliOption>` associated with a short name and optionally a long name, a description and a call-back.

        :param short_name:  Short name. Must be a single alphanum letter
        :param cb: Optional Call-back called when this option is interpreted.
        :param desc: Optional description.
        :param long_name: Optional long name.
        :param param_name: Name of the parameter, if any.
        :return: The new option
        :rtype: CliOption
        """
        new_option=self.dummy_option
        if not isinstance(short_name, str):
            raise CliSyntaxException(
                _message=f'An option short name must be a single alphanumeric character. {short_name} found.')
        if not (desc is None or isinstance(desc, str)):
            raise CliSyntaxException(
                _message=f'An option description must be a string. {short_name} found.')
        if not (cb is None or callable(cb)):
            raise CliSyntaxException(
                _message=f'An option call-back must be callable')

        if len(short_name) == 1 and short_name.isalnum():
            # Check if short_name and long_name already used
            opt_short = self.get_option(short_name)
            opt_long = None
            if long_name is not None and isinstance(long_name,str):
                opt_long = self.get_option_by_long_name(long_name)

            #
            if opt_short is None and opt_long is None:
                new_option=CliOption(short_name, long_name, param_name, desc, cb, None)
                self.options.append(new_option)
                self.need_recalc = True

        else:
            raise CliSyntaxException(_message=f'An option short name must be a single alphanumeric character. {short_name} found.')

        return new_option

    def add_param(self, optional : bool, param_name : str, desc: str) -> bool:
        """Adds a parameter. This parameter may be optional.

        When interpreting the given command line arguments, the non-optional are first
        filled with the strings not identified as options (*i.e* the strings which the
        first character is not :literal:`-`, then the optional parameters are filled.

        :param bool optional: True/False to define an optional/non-optional parameter
        :param str param_name: The name of the parameter
        :param str desc: The description
        :return: True/False if the method succeded/failed
        :rtype: bool
        """
        added = False
        if isinstance(param_name, str):
            param = self.get_param(param_name)
            if param is None:
                if optional:
                    self.optional_params.append(CliParam(param_name, desc))
                else:
                    self.params.append(CliParam(param_name, desc))
                self.need_recalc = True
                added = True

        return added

    def arg_error(self, _title, _error_level):
        self.cb_error("Error. " + _title)
        self.print_syntax(_error_level)

    def get_opt_desc(self):
        pass

    def calc_strings(self):
        """This internal function calculates the *usage* string displayed when invoking the :literal:`-h` option.
        """
        if self.need_recalc:
            self.help_string = "Usage   : " + os.path.basename(sys.argv[0])
            if len(self.options) > 0:
                self.help_string += " [Options]"
            first = True
            for par in self.params:
                self.help_string += " <" + par.name + ">"
            if len(self.optional_params) > 0:
                first = True
                self.help_string += " ["
                for par in self.optional_params:
                    if first:
                        first = False
                    else:
                        self.help_string += ","
                    self.help_string += " <" + par.name + ">"
                self.help_string += " ]"
            if len(self.options) > 0:
                # Calc longest long name
                long_name_max_len = 0
                for opt in self.options:
                    if opt.long_name is not None:
                        long_name_max_len = max(long_name_max_len,len(opt.long_name))

                # Calc longest param name
                param_max_len = 0
                for opt in self.options:
                    if opt.param_name is not None:
                        param_max_len = max(param_max_len,len(opt.param_name))

                self.help_string += "\nOptions :\n"
                for opt in self.options:
                    # Opts
                    self.opt_strings[0]+=opt.short_name
                    if opt.param_name is not None :
                        self.opt_strings[0]+=':'
                    if opt.long_name is not None :
                        long_name=opt.long_name
                        if opt.param_name is not None:
                            long_name+='='
                        self.opt_strings[1].append(long_name)

                    # Help
                    self.help_string += '-' + opt.short_name + ' '

                    if opt.long_name is not None:
                        self.help_string += f' --{opt.long_name}{" "*(long_name_max_len-len(opt.long_name))}'
                    else:
                        self.help_string += f'   {" "*long_name_max_len}'

                    if opt.param_name is not None:
                        self.help_string += f' <{opt.param_name}>{" "*(param_max_len-len(opt.param_name))}'
                    else:
                        self.help_string += f'  {" " * param_max_len} '

                    if opt.desc is not None :
                        self.help_string += ' : ' + opt.desc

                    intepreter = opt.get_interpreter()
                    if opt.param_name is not None and intepreter is not None:
                        self.help_string += (' : ' if opt.desc is None else f'\n   {" "*long_name_max_len}  {" " * param_max_len}       ')
                        self.help_string += f'<{opt.param_name}> is {intepreter.stringify()}'


                    self.help_string += '\n'

            self.need_recalc = False

    def print_syntax(self, _error_level=0):
        """Print syntax summary and exits if `_error_level` is not `None`

        :param _error_level: If not None, exits the program with the value `error_level`.
        """
        self.calc_strings()
        self.cb_info(self.help_string)
        if _error_level is not None:
            sys.exit(_error_level)

    def load(self) -> bool:
        # Unfinished
        loaded = False
        try:
            import jsonpickle
            # TODO
            loaded = True
        except ModuleNotFoundError:
            self.cb_error("Can't load Json parameter file. Module jsonpickle not found")

        return loaded

    def save(self):
        # Unfinished
        saved = False
        try:
            import jsonpickle
            # TODO
            saved = True
        except ModuleNotFoundError:
            self.cb_error("Can't save Json parameter file. Module jsonpickle not found")

        return saved

    def handle(self, cli_args: list, verbose_mode: bool = False, error_level_if_fails: int = 2) -> None:
        """Interprets the command line arguments

        :raises CliSyntaxException:
        :param cli_args: The command line arguments
        :param bool verbose_mode: Allows to log this method
        :param error_level_if_fails: Set the error level used if this method failed
        :return:
        """
        self.calc_strings()
        try:
            import getopt

            all_errs=[]

            try:
                opts, args = getopt.getopt(
                    cli_args,
                    self.opt_strings[0],
                    self.opt_strings[1]
                )
                for opt, opt_arg in opts:
                    if verbose_mode: sys.stdout.write(f"Option {opt}, argument '{str(opt_arg)}'")
                    option = self.get_option(opt)
                    if option is None:
                        option = self.get_option_by_long_name(opt)
                    if option is None:
                        if verbose_mode: sys.stdout.write(f" : NOT FOUND")
                        err_msg = f"Internal error in CliSyntax.handle when handling option {opt}. Not found in internal data"
                        all_errs.append(err_msg)
                        # self.cb_warning(err_msg)
                    else:
                        if verbose_mode: sys.stdout.write(f" : Option '{option.short_name}'")
                        self.found_options += option.short_name
                        option_arg_ok = False
                        try:
                            opt_arg = option.interpret_arg(opt_arg)
                            option_arg_ok = True
                        except CliSyntaxException as err:
                            all_errs.append(err.message)
                            # self.cb_error(err.message)

                        if option_arg_ok:
                            self.found_options_args[option.short_name] = opt_arg
                            if option.cb:
                                if verbose_mode: sys.stdout.write(f", calling call-back with parameter '{opt_arg}'... ")
                                option.cb(opt_arg)

                    if verbose_mode: print()

                arg_max_nb = len(self.params) + len(self.optional_params)
                arg_index = 0
                nb_normal_args = len(self.params)
                for arg in args:

                    if verbose_mode: sys.stdout.write(f"Param  {arg}")
                    if arg_index >= arg_max_nb:
                        err_msg = f" excesses the expected number of parameters"
                        if verbose_mode:
                            sys.stdout.write(err_msg)
                        # all_errs.append(f"Param '{arg}'{err_msg}")
                    self.found_params.append(arg)
                    if arg_index<nb_normal_args:
                        self.params[arg_index].found_val=arg
                    elif arg_index<arg_max_nb:
                        self.optional_params[arg_index-nb_normal_args].found_val = arg
                    arg_index += 1
                    if verbose_mode:
                        print()

                str_err = None
                if arg_index < nb_normal_args:
                    str_err = "Missing parameters. Need "
                    for par in self.params[:nb_normal_args]:
                        str_err += f"{par.name}, "
                    str_err = str_err[0:len(str_err) - 2]

                elif arg_index > arg_max_nb:
                    str_err = "Too many parameters. "
                    for par in self.found_params[arg_max_nb:]:
                        str_err += f"'{par}', "
                    str_err = str_err[0:len(str_err) - 2] + " ignored."
                    str_err+='\n  Found parameters : \n'

                    nb_found_params=len(self.found_params)
                    for param_index in range(0,nb_found_params):
                        if param_index<nb_normal_args :
                            str_err+=f'    {self.params[param_index].name} : {self.params[param_index].found_val}\n'
                        elif param_index<arg_max_nb :
                            str_err+=f'    {self.optional_params[param_index-nb_normal_args].name} : {self.optional_params[param_index-nb_normal_args].found_val}\n'

                if str_err is not None:
                    all_errs.append(str_err)

            except getopt.GetoptError as err_opts:
                all_errs.append(err_opts.msg)

            if len(all_errs)>0:
                for err in all_errs:
                    self.cb_error(err)
                self.print_syntax(error_level_if_fails)

        except ModuleNotFoundError:
            self.cb_error("Can't read the arguments. Module getopt not found")


if __name__ == "__main__" :

    syntax = CliSyntax()
    syntax.add_option('p',long_name="print")
    syntax.add_option('v',desc='verbose',param_name="level").set_interpreter(ArgInterpreterInt(0, 3))
    syntax.add_option('d','debug this stuff')
    syntax.add_option('e', None, 'expand')
    syntax.add_option('s', 'save all this mess', 'save', 'filename', lambda param: print(f'Saving in {param}') )
    syntax.add_option('d', long_name='dump all this mess', cb=lambda param: print(f'Dumping...') )
    syntax.add_option('f', long_name='test-float-1', desc='display float',param_name='float value').set_interpreter(ArgInterpreterFloat(-2))
    syntax.add_option('g', long_name='test-float-2', param_name='float value').set_interpreter(ArgInterpreterFloat(-2))
    syntax.add_option('i', long_name='test-int', desc='int test, no bounds', param_name='int value').set_interpreter(ArgInterpreterInt())
    syntax.add_option('j', long_name='test-int-m', desc='int with min', param_name='int value').set_interpreter(ArgInterpreterInt(min_val=10))
    syntax.add_option('k', long_name='test-int-M', desc='int with max', param_name='int value').set_interpreter(ArgInterpreterInt(max_val=20))
    syntax.add_option('l', long_name='test-int-mM', desc='int with bounds', param_name='int value').set_interpreter(ArgInterpreterInt(min_val=10, max_val=20))
    syntax.add_option('c', desc='choice among primary colors', param_name='list').set_interpreter(ArgInterpreterEnum(('red', 'green', 'blue')))

    syntax.add_param(False,"Command file","Path to the command file")
    syntax.add_param(True,"Optional config file","Path to the config file")
    syntax.add_param(True,"Optional param 2","On s'en fout")

    try :
        syntax.handle(sys.argv[1:], verbose_mode=False, error_level_if_fails=1)
    except CliSyntaxException :
        pass

    print("Found options     : "+str(syntax.found_options))
    print("Found option args : "+str(syntax.found_options_args))
    print("Found params      : "+str(syntax.found_params))
